package default;

/**
 *  Class for different Methods
 */

public class MyClass {
    /**
     * Addition Calculation
     * @param a - First Integer Parameter
     * @param b - Second Integer Paramter
     * @return Sum of the two input integers
     */
    public int add(int a, int b){
    return a + b;
    }

    /**
     * String Concatenate
     * @param a - First String Parameter
     * @param b - Second String Parameter
     * @return Concatenated String
     */
    public String add(String a, String b){
    return a + b;
    }

    /**
     * Subtraction
     * @param a - First Integer Parameter
     * @param b - Second Integer Parameter
     * @return Subtract a by b
     */
    public int sub(int a, int b){
    return a - b;
    }

    /**
     * Multiplication
     * @param a - First Integer Parameter
     * @param b - Second Integer Parameter
     * @return a multiplied by b
     */
    public int mult(int a, int b){
    return a * b;
    }

    /**
     * Division
     * @param a - First Integer Parameter
     * @param b - Second Integer Parameter
     * @return a divided by b
     */
    public int div(int a, int b){
    return a / b;
    }

    /**
     * Division
     * @param a - First Integer Parameter
     * @param b - Second Integer Parameter
     * @return a moded by b
     */
    public int mod(int a, int b){
    return a%b;
    }

    /**
     * Greeting Hello
     * @param name - First String Parameter (name of the person to greet)
     * @return Greet to the named person
     */
    public String sayHello(String name){
    return "Hello" + name;
    }
}